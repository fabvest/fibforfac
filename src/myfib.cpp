#include <iostream>
#include <omp.h>
#include <ctime>
#include <fstream>
#include "myfib.h"
using namespace std;

long unsigned int reqFib(int count){
	if (count == 0) return 0;
	if (count == 1) return 1;
	return reqFib(count - 1) + reqFib(count - 2); 
}

void iterFib(int count){
	long unsigned int mas[count];
	ofstream fout("iterFib.txt");
	double beforeTime = omp_get_wtime();

	mas[0] = 0; mas[1] = 1; mas[2] = 1;
	fout << 0 << " " << mas[0] << " " << 0 << endl; 
	fout << 1 << " " << mas[1] << " " << 0 << endl;

	for(int i = 2; i < count; i++){
		mas[i] = mas[i-1]+mas[i-2];
		

		fout << i 
			<< " " 
			<< mas[i] 
			<< " " 
			<< (omp_get_wtime() - beforeTime) 
			<< endl;  
	}
}


void timeReqFib(int count){
	ofstream fout("reqFib.txt");
	double beforeTime = omp_get_wtime();
	for(int i = 0; i < count; i++){
		fout << i
			<< " "
			<< reqFib(i)
			<< " "
			<< omp_get_wtime() - beforeTime
			<< endl;	
	}
	}

